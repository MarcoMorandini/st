Full potential aerodynamic solver.

See
                                                     
Independent Two-Fields Solution for Full-Potential Unsteady Transonic Flows
A. Parrinello, P. Mantegazza
AIAA Journal, 2010, Vol.48: 1391-1402, 10.2514/1.J050013
                                                           
"Improvements and Extensions to a Full-Potential Formulation Based on Independent Fields"
A. Parrinello, P. Mantegazza
AIAA Journal, 2012, Vol.50: 571-580, 10.2514/1.J051270
                                         
"Automatic Embedding of Potential Flow Wake Surfaces in Generic Monolithic Unstructured Meshes"
A. Parrinello, M. Morandini, P. Mantegazza                                         
Journal of Aircraft, 2013, Vol.50: 1179-1188, 10.2514/1.C032098

"A Two-Fields Full Potential Formulation for Real Gases"
A. Gadda, M. Favale, A. Parrinello, M. Morandini and P. Mantegazza,
AIAA Journal, submitted -> rejected :(