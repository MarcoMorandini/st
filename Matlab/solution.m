function s = solution(s,show_body,show_wake,show_entropy)

tic
disp('Solution...')

%------Coefficients of the integration scheme------%

beta = (4*s.Rho^2 - (1-s.Rho)^2)/(4-(1-s.Rho)^2);

delta = .5*(1-s.Rho^2)/(4-(1-s.Rho)^2);

s.a_0   =  1 - beta;               % a_(n)
s.a_m1  =      beta;               % a_(n-1)
s.b_p1  =.5 + delta;               % b_(n+1)
s.b_0   = (1+beta)/2 - 2*delta;    % b_(n)
s.b_m1  = beta/2 + delta;          % b_(n-1)

%--------------------------------------------------% 

s.min = .01;
s.max = .99/(s.bvdW*s.r0);

s.Cp = zeros(s.l_b,s.step);

s.conv = [];

s.gamma = zeros((s.nn*(s.pan+1))^s.free,1);

if  s.sol>0
    x_p1 = [ones(s.l_unk,1);zeros(s.l_p-s.l_per+s.l_k*s.corr+s.l_w,1)];
    f_p1 = zeros(s.l_p*2+s.l_k*s.corr+s.l_w,1);
    Y = s.Q\x_p1;
else
    s.ro =  ones(s.l_unk,1);
    s.fi = zeros(s.l_unk,1);
    Y = 0;
end

s.K = zeros(s.l_k,1);
s.K_exp = ones(s.l_unk,1);
    
x_m1 = 0;
f_0  = 0;
vv0  = 0;

nrot = s.nb;
s.pw = s.p(s.body,:);

s.IE = [sparse(s.l_equ,s.l_unk)  s.I];

for m = 1:size(s.mode,2)/3;
    s.qq{m} = [0;0];
end
psi = 0;
for j = 1:s.step
    s.j   = j;
    s.eul = j>s.one;
    
    psi = psi + norm(s.wg)*s.dt;
    if  norm(s.wg)*norm(s.vg)*(s.max_rot==0 || (abs(psi)<abs(s.max_rot)*pi/180))>0
        s.vg = rotation(-s.wg*s.dt,s.vg);
        s.Vg = cross(repmat(s.wg,s.l_t,1),[ s.pt zeros(s.l_t,3-s.N)])+repmat(s.vg',s.l_t,1);
        s.Vg = s.Vg(:,1:s.N);
        s.Vb = cross(repmat(s.wg,s.l_b,1),[ s.p(s.body,:) zeros(s.l_b,3-s.N)])+repmat(s.vg',s.l_b,1);
        s.Vb = s.Vb(:,1:s.N);
    end

    if  s.sol>0
        if  s.eul
            x_m1 = x_0;
            vv0 = s.dt*(s.b_0*f_p1+s.b_m1*f_0);
            f_0 = f_p1;
        end
        
        x_0 = x_p1;
        
        vec = s.a_0^s.eul*x_0+s.a_m1*x_m1;
        
        s.b = vv0+[ s.I*vec(1:s.l_unk)
                   [s.IE sparse(s.l_equ,s.l_k*s.corr)]*vec
                    s.uni*vec(s.l_unk+(1:s.l_unk))
                    vec(2*s.l_unk+(1:s.l_k*s.corr)) ];
    end
    
    s.dn = 0;
    s.Vt = 0;
    if  size(s.mode,2)/3>0
        s.pw = [ s.p(s.body,:) zeros(s.l_b,3-s.N) ];
        for m = 1:size(s.mode,2)/3;
            if  s.resp_type==1
                aa = s.magn(j,2*(m-1)+1);
                ap = s.magn(j,2*(m-1)+2);
            else
                f = -s.Cp(:,j-1+(j==1))'*sum([s.nb zeros(s.l_b,3-s.N)].*s.mode(:,(1:3)+3*(m-1)),2);
                qp = [0 1;-[s.mck(3,m) s.mck(2,m)]/s.mck(1,m)]*s.qq{m}+[0;f/s.mck(1,m)];
                s.qq{m} = s.qq{m} + s.dt*qp;
                aa = s.qq{m}(1);
                ap = s.qq{m}(2);
            end
            s.pw = s.pw + aa*s.mode(:,(1:3)+3*(m-1));
            s.Vt = s.Vt + ap*s.mode(:,(1:3)+3*(m-1));
        end
        s.Vt = s.Vt(:,1:s.N);

        if  s.N>2
            nf = cross(s.pw(s.faces(:,2),:)-s.pw(s.faces(:,1),:),...
                       s.pw(s.faces(:,3),:)-s.pw(s.faces(:,1),:));
        else
            nf = [-s.pw(s.faces(:,2),2)+s.pw(s.faces(:,1),2),...
                  +s.pw(s.faces(:,2),1)-s.pw(s.faces(:,1),1) ];
        end
        
        for d = 1:s.N
            nrot(:,d) = s.Mbb*(nf(:,d));
        end
        s.dn = s.nb-nrot;
    end

    for i = 1:s.iter
        [f0,f_p1,s] = residual(s,Y); %s.lin = 1;
        if  s.lin<1 && s.sol>0 && s.sol<3
            s = jacobian(s);
        end
        if  s.sol>0
            [Y,s.conv] = solver(s,Y,f0);
        end
    end
    
    if  s.free
        s.gamma(s.nn+1:end) = s.gamma(1:end-s.nn);
    end
    
    if  s.sol>0
        x_p1 = s.Q*Y;
        s.ro = x_p1(1:s.l_unk);
        s.fi = x_p1(s.l_unk+1:2*s.l_unk);
        s.fi = s.fi-mean(s.fi);
    end
    
    rr = s.ro(s.body-s.l_per);
    pp = (s.P0+s.avdW*s.r0^2)*(rr.*(1-s.r0*s.bvdW)./(1-rr*s.r0*s.bvdW)).^s.g - s.avdW*(s.r0*rr).^2;

    s.Cp(:,j) = 2*(real(pp)-s.P0)'/(s.r0*s.V0^2);
    
    visualization(s,show_body,show_wake,show_entropy);

end
disp(['Done in ' num2str(toc,3) ' seconds']); disp(' ')

s.CFX = -s.Cp'*nrot(:,1);
s.CFY = -s.Cp'*nrot(:,2);
s.CMZ = -s.Cp'*((s.pw(:,1)-(s.pole(1))).*nrot(:,2) - (s.pw(:,2)-s.pole(2)).*nrot(:,1));

if  s.N>2
    s.CFZ = -s.Cp'*nrot(:,3);
    s.CMX = -s.Cp'*((s.pw(:,2)-(s.pole(2))).*nrot(:,3) - (s.pw(:,3)-s.pole(3)).*nrot(:,2));
    s.CMY = -s.Cp'*((s.pw(:,3)-(s.pole(3))).*nrot(:,1) - (s.pw(:,1)-s.pole(1)).*nrot(:,3));
end

scrsz = get(0,'screensize');

figure('position',[scrsz(3)*.2 scrsz(4)*.1 scrsz(3)*.5 scrsz(4)*.8])
if  s.N>2
    ax(1) = subplot(6,1,1);
    plot((1:s.step)*s.dt,s.CFX)
    ylabel('Fx/q','fontsize',14)
    ax(2) = subplot(6,1,2);
    plot((1:s.step)*s.dt,s.CFY)
    ylabel('Fy/q','fontsize',14)
    ax(3) = subplot(6,1,3);
    plot((1:s.step)*s.dt,s.CFZ)
    ylabel('Fz/q','fontsize',14)
    ax(4) = subplot(6,1,4);
    plot((1:s.step)*s.dt,s.CMX)
    ylabel('Mx/q','fontsize',14)
    ax(5) = subplot(6,1,5);
    plot((1:s.step)*s.dt,s.CMY)
    ylabel('My/q','fontsize',14)
    ax(6) = subplot(6,1,6);
    plot((1:s.step)*s.dt,s.CMZ)
    ylabel('Mz/q','fontsize',14)
    xlabel('t','fontsize',16)
    linkaxes(ax,'x');
else 
    ax(1) = subplot(3,1,1);
    plot((1:s.step)*s.dt,s.CFX)
    ylabel('Fx/q','fontsize',14)
    ax(2) = subplot(3,1,2);
    plot((1:s.step)*s.dt,s.CFY)
    ylabel('Fy/q','fontsize',14)
    ax(3) = subplot(3,1,3);
    plot((1:s.step)*s.dt,s.CMZ)
    ylabel('Mz/q','fontsize',14)
    xlabel('t','fontsize',16)
    linkaxes(ax,'x');
end