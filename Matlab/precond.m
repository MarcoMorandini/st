function s = precond(s)

if  s.sol>0

tic, disp('Factorization...')

grad = - spdiags(s.Vg(:,1),0,s.l_t,s.l_t)*s.G{1};
for d = 2:s.N
    grad = grad - spdiags(s.Vg(:,d),0,s.l_t,s.l_t)*s.G{d};
end
S_fi = grad(s.wlo,:)-grad(s.wup,:);
E_fi = -s.II*grad;

I_p  = sparse([1:s.l_p-s.l_per s.wake'-s.l_per],[1:s.l_p-s.l_per s.WAKE'-s.l_per],s.VOL2,s.l_equ,s.l_unk);
s = rmfield(s,'VOL2');

Mg = sparse(repmat(1:s.l_t,1,s.N+1),s.v_k-s.l_per,ones(1,s.l_t*(s.N+1))/(s.N+1),s.l_t,s.l_unk);

AA = -s.M_x{1}*spdiags(s.Vg(:,1),0,s.l_t,s.l_t);
for d = 2:s.N
    AA = AA - s.M_x{d}*spdiags(s.Vg(:,d),0,s.l_t,s.l_t);
end

F_ro = AA*(Mg-s.eps_sub*s.V0/s.c0*spdiags(s.Lrif./sqrt(sum(s.Vg.^2,2)),0,s.l_t,s.l_t)*grad);
bb = full(sum(F_ro,2)); bb(s.wake-s.l_per) = bb(s.wake-s.l_per)/2;
F_ro_p = sparse([1:s.l_equ s.wake'-s.l_per],[1:s.l_equ s.WAKE'-s.l_per],[bb ; bb(s.wake-s.l_per)],s.l_equ,s.l_unk);

% F_ro_p(s.far,:) = 0;
% s.F_fi(s.far,:) = 0;

mod = sqrt(sum(s.nf.^2,2));
% vet = -mod./(s.c0/s.V0-s.nf(:,1)./mod);
% vet = -mod./(1/.2-s.nf(:,1)./mod);
% vet = -mod./(s.c0-sum(s.nf.*s.Vf(:,1:s.N),2)./mod);
vet = -mod./s.c0;

s.I_fi = 0*sparse(s.far-s.l_per,s.far-s.l_per,vet,s.l_equ,s.l_unk);

E_K  = [];
S_K  = s.c0^2*(s.I_K(s.WAKE-s.l_per,1:s.l_k*s.corr)-s.I_K(s.wake-s.l_per,1:s.l_k*s.corr))/(s.g-1);
T_K  = sparse(s.l_k*s.corr,s.l_k*s.corr);
T_fi = sparse(s.l_k*s.corr,s.l_unk);
if  s.corr
    V = -s.Vg(s.IND,:);
    V = V - spdiags(sum(s.nk.*V,2),0,s.l_k,s.l_k)*s.nk;
    for d = 1:s.N
        T_K  = T_K  - (spdiags(V(:,d),0,s.l_k,s.l_k)*s.G{d}(s.IND,:))*s.I_K;
        T_fi = T_fi - spdiags(sum(s.G{d}(s.IND,:),2),0,s.l_k,s.l_k)*s.grad_vn{d};
    end
    E_K  = -s.c0^2*I_p*s.I_K/(s.g-1);
end
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if  s.avdW*s.bvdW==0  % ideal gas model
    E_ro = -I_p*s.c0^2; 
else   % Van der Waals gas model
    K1 = (s.P0+s.r0^2*s.avdW)/(s.r0*(s.g-1));
    K2 = 2*s.avdW*s.r0;
%     K3 = (1-s.bvdW)^s.g/s.r0;
    b  = s.r0*s.bvdW;
    E_ro = ( K1*s.g*(1-(s.g-b)/(1-b)) + K2 )*I_p;        
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
aa = [sparse(s.l_equ,s.l_unk)  I_p];
bb = [E_ro  E_fi];

A = [ I_p                    s.I_fi  sparse(s.l_equ,s.l_k*s.corr)       
     s.uni    sparse(s.l_w,s.l_unk)  sparse(s.l_w  ,s.l_k*s.corr) 
                   aa                sparse(s.l_equ,s.l_k*s.corr)        
     sparse(s.l_w,s.l_unk)    s.uni  sparse(s.l_w  ,s.l_k*s.corr)        
     sparse(s.l_k*s.corr,2*s.l_unk)   speye(s.l_k*s.corr) ]; 
    
B = [ F_ro_p           s.F_fi  sparse(s.l_equ,s.l_k*s.corr)       
      sparse(s.l_w,s.l_unk*2)  sparse(s.l_w  ,s.l_k*s.corr) 
                  bb                    E_K        
           -s.uni*s.c0^2       S_fi     S_K        
      sparse(s.l_k*s.corr,s.l_unk) T_fi   T_K ]; 
  
[s.L,s.U,s.P,s.Q] = lu(A-s.dt*B);

s = rmfield(s,'F_fi');
disp(['Done in ' num2str(toc,3) ' seconds']); disp(' ')
end