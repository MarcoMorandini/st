TRANSonic POtential baSED Solver -> TRANSPOSED S -> S^T -> ST

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The ST code is far to be perfect because the model has a weakness, the wake.
In my experience, a proper description of the wake is crucial not for accuracy but for robustness.
So, take care of wake geometry and discretization during the mesh generation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The main function "ST" expects four input arguments and collects all outputs in a structure:

s = ST(input_file_name,show_body_flag,show_wake_flag,show_entropy_flag);

s is the output structure

the input file collects all the information required for the analysis (except modal data)

show_body_flag:     1 -> Plots the Pressure coefficient (Cp) on the body surface
                    2 -> Plots the Mach number on the body surface
                    3 -> Plots the density on the body surface (3D) Plots the density on the whole domain (2D)
                    0 -> Nothing
                    
show_wake_flag:     1 -> Plots the value of the potential function at wake nodes along wake normals
                    0 -> Nothing
                    
show_entropy_flag:  1 -> Plots the value of the potential function at wake nodes
                    0 -> Nothing   
                    
s = ST(input_file_name,0,0,0); for faster calculations                          

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

INPUT FILE

name_of_mesh_mat_file:                         the .mat file must contain the next 5 entities and other 2 for periodic boundary conditions
name_of_nodal_coordinates_matrix:              [ x y [z]]
name_of_connectivity_matrix:                   Triangles for 2D domains , Tetrahedrons for 3D domains
name_of_wall_boundary_nodes_list:              impermeable wall
name_of_wake_boundary_nodes_list:              in 3D domains internal wake boundaries (lines) must be excluded from the list          
name_of_exterior_boundary_nodes_list:          far-field boundary
name_of_first__periodic_boundary_nodes_list:   same order of the second one (the preprocessing tries to order it but pay attention)
name_of_second_periodic_boundary_nodes_list:   same order of the first  one (the preprocessing tries to order it but pay attention)
heat_capacity_ratio:                           cp/cv for ideal gas model
attraction_van_der_Waals_coefficient:          normalized , typical value: 3
excluded_volume_van_der_Waals_coefficient:     normalized , typical value: 1/3
asymptotic_pressure:                           dimensional
asymptotic_density:                            dimensional
subsonic_dissipation_coefficient:              local_upwind ~ eps_sub*local_Mach + eps_sup*real(sqrt( local_Mach^2 - 1 )) 
supersonic_dissipation_coefficient:            local_upwind ~ eps_sub*local_Mach + eps_sup*real(sqrt( local_Mach^2 - 1 ))
grid_velocity_vector:                          dimensional
grid_angular_velocity_vector:                  [rad/time]
reference_pole_coordinates:                    for moment coefficients calculation         
maximum_grid_rotation_in_deg:                  ignored if 0
wake_treatment_flag:                           0 -> usual wake , 1-> diffused wake (wake nodes list can be limited to trailing edge; works only with null grid angular velocity)
ALE_scheme_flag:                               fluxes computed with: 1 -> relative velocity , 2 -> gradients 
entropy_correction_flag:                       0 -> OFF , 1 -> ON    (not so effective and robust for 3D domains)
divergence_of_deformed_grid_flag:              0 -> deformation with null divergence , 1 -> deformation with divergence>0         
time_step:                                     dimensional time step
number_of_total_time_steps:                    number of time steps
number_of_initial_Euler_time_steps:            only Euler time steps if <2  
number_of_Newton_Raphson_iterations:           fixed number of NR iterations
asymptotic_root_of_two-step_scheme:            in the range [0,1]:  0 -> L-stable Backward Difference Formula  ;  1 -> A-stable Crank-Nicolson rule
solution_method_flag:                          0 -> explicit integration , 1 -> direct solution , 2 -> GMRES , 3 -> GMRES Matrix-free
GMRES_tolerance:                               GMRES residual tolerance on norm(b-A*x)/norm(b)
number_of_GMRES_internal_iterations:           dimension of Krylov subspace              
number_of_GMRES_external_iterations:           maximum number of restarts
modes_file_name:                               modal_filename.mod (load modal shapes stored in the file "modal_filename.mod"
selected_modes:                                integer numbers separated by space
response_type:                                 0 -> free response (load modal_filename.mck file) , 1 -> forced response 
							(load modal_filename.magn file)

      
.mod file format for n modes (each row corresponds to and aerodynamic node)
Dx1 Dy1 Dz1 ..... Dxi Dyi Dzi ..... Dxn Dyn Dzn
Dx1 Dy1 Dz1 ..... Dxi Dyi Dzi ..... Dxn Dyn Dzn
Dx1 Dy1 Dz1 ..... Dxi Dyi Dzi ..... Dxn Dyn Dzn

.mck file format for n modes     
m1 ... mi ... mn       
c1 ... ci ... cn       
k1 ... ki ... kn

.magn file format for n nodes (each row corresponds to a different time step)
q1_T1       qdot1_T1      ....  qi_T1       qdoti_T1      ....  qn_T1       qdotn_T1     
q1_T2       qdot1_T2      ....  qi_T2       qdoti_T2      ....  qn_T2       qdotn_T2     
q1_T3       qdot1_T3      ....  qi_T3       qdoti_T3      ....  qn_T3       qdotn_T3     
   .           .          ....     .           .          ....     .           .         
   .           .          ....     .           .          ....     .           .         
q1_Tsteps   qdot1_Tsteps  ....  qi_Tsteps   qdoti_Tsteps  ....  qn_Tsteps   qdotn_Tsteps 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

REFERENCES

--------------------General approach and implementation---------------------
                                                     
Independent Two-Fields Solution for Full-Potential Unsteady Transonic Flows
A. Parrinello, P. Mantegazza
AIAA Journal, 2010, Vol.48: 1391-1402, 10.2514/1.J050013


-----------------Entropy correction and solution procedure------------------
                                                           
"Improvements and Extensions to a Full-Potential Formulation Based on Independent Fields"
A. Parrinello, P. Mantegazza
AIAA Journal, 2012, Vol.50: 571-580, 10.2514/1.J051270


-------------------------------Wake treatment-------------------------------
                                         
"Automatic Embedding of Potential Flow Wake Surfaces in Generic Monolithic Unstructured Meshes"
A. Parrinello, M. Morandini, P. Mantegazza                                         
Journal of Aircraft, 2013, Vol.50: 1179-1188, 10.2514/1.C032098

                            
----Van der Waals potential formulation and periodic boundary conditions----

"A Two-Fields Full Potential Formulation for Real Gases"
A. Gadda, M. Favale, A. Parrinello, M. Morandini and P. Mantegazza,
AIAA Journal, submitted.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CONTACTS

andrea.parrinello@polimi.it
marco.morandini@polimi.it
paolo.mantegazza@polimi.it

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EXAMPLES 

addpath('TestCases')       
 
s = ST('naca0012.dat',1,0,0);             transonic with entropy correction

s = ST('naca0012_periodic.dat',2,1,0);    periodicity along wind direction (the wake exits and enters from the periodic boundaries)
   
s = ST('compressor_blade.dat',3,0,0);     transonic and periodic (http://www.rpmturbo.com/testcases/sc10/index.html)

s = ST('turbine_blade.dat',3,0,0);        transonic and periodic

s = ST('wing.dat',2,1,0);                 transonic (try to change the wake treatment) 
   
s = ST('rotor_blade.dat',2,1,0);          with angular velocity

s = ST('small_blade.dat',2,1,0);          with angular velocity and forced deformation

clear all; s = ST('blade_1_of_8.dat',2,1,0); show_all_blades;  with angular velocity and periodicity along wind direction  (5Gb of free RAM required) 
                                                                           
