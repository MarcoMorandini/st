function s = ST(name,show_body,show_wake,show_entropy)

s = preprocessing(name);
s = freewake(s);
s = precond(s);
s = solution(s,show_body,show_wake,show_entropy);

end