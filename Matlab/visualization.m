function visualization(s,show_body,show_wake,show_entropy)

if show_body+show_wake+show_entropy>0

if  s.j==1
    figure
end

hold off
if  s.N>2
    if  show_body>0
        switch show_body
            case 1
            trisurf(s.faces,s.pw(:,1),s.pw(:,2),s.pw(:,3),s.Cp(:,s.j),'EdgeColor','none','FaceColor','interp','Facealpha',.8); hold on; tt = '\color{blue}Cp  ';
            case 2
            trisurf(s.faces,s.pw(:,1),s.pw(:,2),s.pw(:,3),s.Mn,'EdgeColor','none','FaceColor','interp','Facealpha',.8); hold on; tt = '\color{blue}Mach  ';
            case 3
            trisurf(s.faces,s.pw(:,1),s.pw(:,2),s.pw(:,3),s.ro(s.body-s.l_sym),'EdgeColor','none','FaceColor','interp','Facealpha',.8); hold on; tt = '\color{blue}Density  ';
        end
        if  norm(s.wg)*norm(s.vg)>0
            quiver3(0,0,0,s.vg(1)/norm(s.vg),s.vg(2)/norm(s.vg),s.vg(3)/norm(s.vg))
        end
        t = colorbar('peer',gca,'location','southoutside'); axis equal
        set(get(t,'ylabel'),'String',tt,'rotation',0,'fontsize',16,'HorizontalAlignment','right','verticalAlignment','middle');
    end
    if  show_wake
        if  s.free
            w = (s.fi(s.node)-mean(s.fi(s.node)))/30;
            plot3(s.p(s.node,1)+s.nw(1,1)*w,s.p(s.node,2)+s.nw(1,2)*w,s.p(s.node,3)+s.nw(1,3)*w,'.b'); hold on
        else
            w = s.fi(s.wake-s.l_per)/30;
            W = s.fi(s.WAKE-s.l_per)/30;
            plot3(s.p(s.wake,1)+s.nw(:,1).*w,s.p(s.wake,2)+s.nw(:,2).*w,s.p(s.wake,3)+s.nw(:,3).*w,'.r',...
                  s.p(s.wake,1)+s.nw(:,1).*W,s.p(s.wake,2)+s.nw(:,2).*W,s.p(s.wake,3)+s.nw(:,3).*W,'.b'); hold on
        end
        axis equal
    end
    if  s.corr && show_entropy
        plot3(s.p(s.wake,1),s.p(s.wake,3),s.K(s.l_w+(1:s.l_w)),'.g',...
              s.p(s.wake,1),s.p(s.wake,3),s.K(length(s.body2)+(1:s.l_w)+s.l_w),'.r'); hold on
        plot3(s.p(s.body2,1),s.p(s.body2,3),s.K(1:length(s.body2)),'.k')    
    end
    axis off
    view([-5 4 3])
    rotate3d on
else
    xm = min(s.p(s.body,1));
    switch show_body
        case 1
        triplot(s.faces(:,[1:2 1]),s.p(s.body,1)-xm,-s.Cp(:,s.j),'k','linewidth',2); hold on
        xlim([0 max(s.p(s.body,1))-xm]);
        ylabel('-Cp','fontsize',14)
        case 2
        triplot(s.faces(:,[1:2 1]),s.p(s.body,1)-xm,s.Mn,'k','linewidth',2); hold on
        xlim([0 max(s.p(s.body,1))-xm]);
        ylabel('Mach','fontsize',14)
        case 3
        if  ~isempty(s.per_lo)
            roro = s.ro([s.per_up'-s.l_per 1:s.l_p-s.l_per]);
            dx = s.p(s.per_up(1),1)-s.p(s.per_lo(1),1);
            dy = s.p(s.per_up(1),2)-s.p(s.per_lo(1),2);
            trisurf(s.t,s.p(:,1)-xm+dx*0,s.p(:,2)+dy*0,s.p(:,2)*0,roro*s.r0,'EdgeColor','interp','FaceColor','interp');hold on;
            trisurf(s.t,s.p(:,1)-xm+dx*1,s.p(:,2)+dy*1,s.p(:,2)*0,roro*s.r0,'EdgeColor','interp','FaceColor','interp');
            trisurf(s.t,s.p(:,1)-xm+dx*2,s.p(:,2)+dy*2,s.p(:,2)*0,roro*s.r0,'EdgeColor','interp','FaceColor','interp');
        else         
            trisurf(s.t,s.p(:,1)-xm,s.p(:,2),s.p(:,2)*0,s.ro(1:s.l_p)*s.r0,'EdgeColor','interp','FaceColor','interp');hold on;
            axis([-1 2 -1 1])
        end
        t = colorbar('peer',gca,'location','southoutside');
        set(get(t,'ylabel'),'String','\color{blue}Density  ','rotation',0,'fontsize',16,'HorizontalAlignment','right','verticalAlignment','middle');
        view(2); axis off; axis equal;
    end
    if  show_wake
        if  s.free
            plot(s.p(s.node,1)-xm,s.fi(s.node)/40,'.r'); hold on
        else
            plot(s.p(s.wake,1)-xm,s.p(s.wake,2)+s.fi(s.wake-s.l_per)/40,'.b',...
                 s.p(s.wake,1)-xm,s.p(s.wake,2)+s.fi(s.WAKE-s.l_per)/40,'.g'); hold on
        end
        xlim([min([-1;s.p([s.wake;s.w_no;s.w_si],1)]) min([ max(s.p(s.body,1))+9 max(s.p(s.wake,1))])])
    end
    if  s.corr && show_entropy
        plot(s.p([s.body2;s.wake;s.wake],1)-xm,s.K,'.k'); 
        axis([-1 max(s.p(s.body,1))-xm+10 0 .01])
    end
end
title(['t = ' num2str(s.j*s.dt)],'fontsize',14)
pause(1e-3)
end