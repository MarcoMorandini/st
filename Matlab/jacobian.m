function  s = jacobian(s)

grad = spdiags(s.V(:,1)-s.Vg(:,1),0,s.l_t,s.l_t)*s.G{1};
BB   = s.M_x{1}*spdiags(s.V(:,1)-s.Vg(:,1),0,s.l_t,s.l_t);
for d = 2:s.N
    grad = grad + spdiags(s.V(:,d)-s.Vg(:,d),0,s.l_t,s.l_t)*s.G{d};
    BB   = BB   + s.M_x{d}*spdiags(s.V(:,d)-s.Vg(:,d),0,s.l_t,s.l_t);
end

Mg = sparse(repmat(1:s.l_t,1,s.N+1),double(s.v_k)-s.l_per,ones(1,s.l_t*(s.N+1))/(s.N+1),s.l_t,s.l_unk);

F_ro = BB*(Mg-spdiags(s.upw./s.sv,0,s.l_t,s.l_t)*grad);

% mod = sqrt(sum(s.nf.^2,2));
% vet = -mod./(1/.2-s.nf(:,1)./mod);
% vet = -mod./(s.c0+sum(s.nf.*s.Vf(:,1:s.N),2)./mod);
% s.I_fi = 1*sparse(s.far-s.l_per,s.far-s.l_per,vet,s.l_equ,s.l_unk);

if ~isfield(s,'M_f')  
for d = 1:s.N
    vv(:,d) = reshape(s.n(:,:,d),s.l_t*(s.N+1),1)./repmat(s.Lrif.^s.N,s.N+1,1);
end  
for i = 1:s.N+1
    s.M_f{i} = sparse(repmat(1:s.l_t,1,s.N+1),double(s.v_k)-s.l_per,sum(vv.*squeeze(s.n(repmat(1:s.l_t,1,s.N+1),i,:)),2),s.l_t,s.l_unk);
end
end

F_fi = sparse(s.l_equ,s.l_unk);
for i = 1:s.N+1
    F_fi = F_fi + sparse(double(s.t3(:,i)-s.l_per),1:s.l_t,s.RO ,s.l_equ,s.l_t)*s.M_f{i};
end

S_ro =  0*sparse([1:s.l_w 1:s.l_w],[s.wake;s.WAKE]-s.l_per,...
      [-s.K_exp(s.wake-s.l_per).*s.ro(s.wake-s.l_per).^(s.g-2)
       +s.K_exp(s.WAKE-s.l_per).*s.ro(s.WAKE-s.l_per).^(s.g-2)],s.l_w,s.l_unk)*s.c0^2;

S_fi = grad(s.wlo,:)-grad(s.wup,:);

E_K  = [];
S_K  = [];
T_K  = sparse(s.l_k*s.corr,s.l_k*s.corr);
T_fi = sparse(s.l_k*s.corr,s.l_unk);
if  s.corr
    for d = 1:s.N
        T_K  = T_K  - spdiags(s.VI(:,d),0,s.l_k,s.l_k)*s.G{d}(s.IND,:)*s.I_K;
        T_fi = T_fi - spdiags(s.G{d}(s.IND,:)*s.K_exp,0,s.l_k,s.l_k)*s.grad_vn{d};
    end

    E_K  = -s.c0^2*spdiags(s.ro(1:s.l_p-s.l_per).^(s.g-1),0,s.l_p-s.l_per,s.l_p-s.l_per)*s.I*s.I_K/(s.g-1);

    S_K  = (spdiags(s.ro(s.WAKE-s.l_per).^(s.g-1),0,s.l_w,s.l_w)*s.I_K(s.WAKE-s.l_per,:)-...
            spdiags(s.ro(s.wake-s.l_per).^(s.g-1),0,s.l_w,s.l_w)*s.I_K(s.wake-s.l_per,:))*s.c0^2/(s.g-1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r = s.ro(1:s.l_equ);
if  s.avdW*s.bvdW==0  % ideal gas model
    dia = spdiags(s.K_exp(1:s.l_equ).*r.^(s.g-2),0,s.l_equ,s.l_equ);
    E_ro = s.c0^2*dia*s.I;
else   % Van der Waals gas model
    b  = s.r0*s.bvdW;
    K1 = (s.P0+s.r0^2*s.avdW)/(s.r0*(s.g-1));
    K2 = 2*s.avdW*s.r0;
    K3 = (1-b)^s.g;
    dia = spdiags(-K1*K3*s.g./r.^2.*(1./r-b).^(-s.g).*(1-(s.g-r*b)./(1-r*b))-K2,0,s.l_equ,s.l_equ);
    E_ro = dia*s.I;        
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

aa = [sparse(s.l_equ,s.l_unk)  s.I];
bb = [-E_ro  -s.II*grad];

A = [           s.I           s.I_fi  sparse(s.l_equ,s.l_k*s.corr) 
               s.uni            sparse(s.l_w,s.l_unk+s.l_k*s.corr)
                 aa             sparse(s.l_equ,s.l_k*s.corr)
       sparse(s.l_w,s.l_unk)      s.uni sparse(s.l_w,s.l_k*s.corr)
       sparse(s.l_k*s.corr,s.l_unk*2)          speye(s.l_k*s.corr)];

B = [          F_ro           F_fi sparse(s.l_equ,s.l_k*s.corr)
        sparse(s.l_w,s.l_unk)   sparse(s.l_w,s.l_unk+s.l_k*s.corr)
                        bb                           E_K
               S_ro                S_fi              S_K
     sparse(s.l_k*s.corr,s.l_unk)   T_fi             T_K   ];
             
s.AA = s.P*((A - s.dt*B))*s.Q;