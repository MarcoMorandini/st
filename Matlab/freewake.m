function s = freewake(s)

dx = (max(-s.p*s.vg(1:s.N))-min(-s.p(s.wake,:)*s.vg(1:s.N)))/s.V0;
if  s.step*s.dt<dx
    s.pan = s.step;  % number of along-wind wake panels 
else
    s.pan = max([2 ceil(dx/s.dt)]);
end

if  s.free
    
t_tot = tic;
disp('Wake...')    
    
delta = repmat(-s.dt*s.vg',2,1);

tri = [ 1 2 1+2    ; 2+2    1+2    2 ];
tri2= [ 1 2 1+s.nn ; 2+s.nn 1+s.nn 2 ];

ind_i = [];
ind_j = [];
ind_a = [];

dd  = [2:3 1:3];

for i = s.ind_te
    
    [rig,~] = size(s.inter{i});
    
    if  s.N>2
        coor = [ s.xte(i:i+1,:)-delta ; s.xte(i:i+1   ,:) ];
    else
        tt = [ s.xte(1:2) -.5
               s.xte(1:2) +.5 ];
        
        coor = [ tt-delta ; tt ];
    end
        
    v1  = coor(2,:)-coor(1,:);
    v2  = coor(3,:)-coor(2,:);
    mod = sqrt((v1(2)*v2(3)-v1(3)*v2(2))^2+(v1(3)*v2(1)-v1(1)*v2(3))^2+(v1(1)*v2(2)-v1(2)*v2(1))^2);

    v1 = coor(      3 ,:)-coor(      2 ,:);
    v2 = coor(tri(:,2),:)-coor(tri(:,1),:);
        
    for j = 1:s.pan    
        
        coor = coor+repmat(delta,2,1);
           
        ss23 = zeros(rig,1);
        co = coor(2,:);
        for d = 1:s.N
            ss23 = ss23+(v1(dd(d  ))*(s.inter{i}(:,dd(d+1))-co(dd(d+1)))-...
                         v1(dd(d+1))*(s.inter{i}(:,dd(d  ))-co(dd(d  ))))*s.nfree(i,d);
        end
        ind{1} = find(ss23>=0);
        ind{2} = find(ss23<0 );

        ss23 = abs(ss23);
        for jj = 1:2    
            if ~isempty(ind{jj})
            co = coor(tri(jj,1),:);
            v3 = s.inter{i}(ind{jj},:);
            ss12 = zeros(length(ind{jj}),1);
            for d = 1:s.N
                ss12 = ss12+(v2(jj,dd(d  ))*(v3(:,dd(d+1))-co(dd(d+1)))-...
                             v2(jj,dd(d+1))*(v3(:,dd(d  ))-co(dd(d  ))))*s.nfree(i,d);
            end  
            ss = [ mod-ss23(ind{jj})-ss12 ss12];              
            meno = find(sum(sign(ss),2)>0);
            ll = length(meno)*3;
            if  ll>0  
                ind_i = [ind_i;repmat(s.lati{i}(ind{jj}(meno)),3,1)];
                ind_j = [ind_j;reshape(repmat(tri2(jj,:)+(i-1)+s.l_w*(j-1),ll/3,1),ll,1)];
                ind_a = [ind_a;repmat(s.segno{i}(ind{jj}(meno))/mod,3,1).*reshape([ss23(ind{jj}(meno)) ss(meno,:)],ll,1)];
            end  
            end
        end
    end
end

GL = sparse(ind_i,ind_j,ind_a,s.l_l,s.nn*(s.pan+1));

GN = sparse(double([s.edge(:,1);s.edge(:,2)]),[1:s.l_l 1:s.l_l],[-ones(s.l_l,1);ones(s.l_l,1)],s.l_unk,s.l_l);

count = full(sum(abs(GN)*abs(GL),2));
count(count==0) = 1;

Gg = spdiags(.5./count,0,s.l_unk,s.l_unk)*GN*GL;

s.node = unique([s.edge(ind_i,1);s.edge(ind_i,2);s.wake]);

p2 = ones(s.l_p,1);
p2(s.node) = 0;
s.ele = find(sum(p2(s.t),2)==0);

p2 = zeros(s.l_p,1);
p2(s.wake) = 1;
altri = find(sum(p2(s.t),2)>0);

s.no  = intersect(s.ele,altri);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if 1
    
    s.ele = setdiff(s.ele,altri);
    
    ind_i = [];
    ind_j = [];
    ind_a = [];
    for i = 1:length(s.no)
        nodi = intersect(reshape(s.t(s.no(i),:),s.N+1,1),s.wake);
        ll = length(nodi);
        ind_i = [ ind_i ; i*ones(ll,1) ];
        ind_j = [ ind_j ;     nodi     ];
        ind_a = [ ind_a ; ones(ll,1)/ll];
    end
    MM2 = sparse(ind_i,double(ind_j),ind_a,length(s.no),s.l_equ);
    
    MTE = MM2*s.MM1;
    
    for d = 1:s.N
        s.G{d}(s.no,:) = MTE*s.G{d}(s.on,:);
    end

else
    
    for d = 1:s.N
        s.G2{d} = s.G{d};
        s.G2{d}(s.no,s.wake) = (s.G2{d}(s.no,s.wake)+s.G2{d}(s.no,s.WAKE))/2;
        s.G2{d}(s.no,s.WAKE) =  s.G2{d}(s.no,s.wake);
    end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pp = sparse(s.ele,s.ele,ones(length(s.ele),1),s.l_t,s.l_t);

for d = 1:s.N
    s.GG{d} = pp*s.G{d}*Gg;
end

if  s.pan<2
    k = 1;
    pp = sparse([    1:s.l_s       1:s.l_s       s.l_s+(1:s.l_s) s.l_s+(1:s.l_s)],...
                [     s.wake    ;   s.WAKE      ;    s.wake     ;    s.WAKE     ],...
                [ ones(s.l_s,1) ;-ones(s.l_s,1) ; ones(s.l_s,1) ;-ones(s.l_s,1) ],s.l_s*2,s.l_unk);
    for d = 1:s.N
        s.GG{d} = s.G{k,d}-s.GG{d}*pp;
    end
end

disp(['Done in ' num2str(toc(t_tot),3) ' seconds'])
disp(' ')

end