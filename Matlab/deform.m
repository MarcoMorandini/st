function  s = deform(s)

s.n = normals(s.p,s.t);

pw = s.p(s.body,:);
pf = s.p(s.far ,:);
     
if  s.N>2
    nfb = cross(pw(s.faces(:,2),:)-pw(s.faces(:,1),:),...
                pw(s.faces(:,3),:)-pw(s.faces(:,1),:));
    nff = cross(pf(s.fac_f(:,2),:)-pf(s.fac_f(:,1),:),...
                pf(s.fac_f(:,3),:)-pf(s.fac_f(:,1),:));
else
    nfb = [-pw(s.faces(:,2),2)+pw(s.faces(:,1),2),...
           +pw(s.faces(:,2),1)-pw(s.faces(:,1),1) ];
    nff = [-pf(s.fac_f(:,2),2)+pf(s.fac_f(:,1),2),...
           +pf(s.fac_f(:,2),1)-pf(s.fac_f(:,1),1) ]; 
end
       
for d = 1:s.N        
    s.nb(:,d) = s.Mbb*(nfb(:,d));
    s.nf(:,d) = s.Mff*(nff(:,d));
end

if  s.div || ~isfield(s,'I')  
    det  = sum(reshape(s.p(s.t,1),s.l_t,s.N+1).*s.n(:,:,1),2);
    s.II = sparse(s.v_h-s.l_per,s.v_i,s.E_v*det,s.l_equ,s.l_t);
    s.JJ = sparse(s.v_k-s.l_per,s.v_i,s.E_v*det,s.l_unk,s.l_t);
    s.I  = sparse(s.v_H-s.l_per,s.v_K-s.l_per,s.E_p*det,s.l_equ,s.l_unk); 
    s.Lrif = det.^(1/s.N);
end

if  ~isfield(s,'L') && s.sol>0
    s.F_fi = sparse(s.l_equ,s.l_unk); 
end

if  s.div || ~isfield(s,'G')
    for d = 1:s.N
        s.G{d} = sparse(s.v_i,s.v_k-s.l_per,reshape((s.n(:,:,d))./...
                   repmat(det,1,s.N+1),s.l_t*(s.N+1),1),s.l_t,s.l_unk);
        s.M_x{d} = sparse(s.l_equ,s.l_t);
        for j = 1:s.N+1
            s.M_x{d} = s.M_x{d}+sparse(s.t3(:,j)-s.l_per,1:s.l_t,s.n(:,j,d),s.l_equ,s.l_t);
        end
        if  ~isfield(s,'L') && s.sol>0
            s.F_fi = s.F_fi + s.M_x{d}*s.G{d};
        end
    end
end

if  s.corr
    nn = reshape(s.n(s.IND,:,:),s.l_k*(s.N+1),s.N);
    s.nk = spdiags(1./sqrt(sum(nn(s.indk,:).^2,2)),0,s.l_k,s.l_k)*nn(s.indk,:);
    grad = spdiags(s.nk(:,1),0,s.l_k,s.l_k)*s.G{1}(s.IND,:);
    for d = 2:s.N
        grad = grad + spdiags(s.nk(:,d),0,s.l_k,s.l_k)*s.G{d}(s.IND,:);
    end
    for d = 1:s.N
        s.grad_vn{d} = s.G{d}(s.IND,:)-spdiags(s.nk(:,d),0,s.l_k,s.l_k)*grad;
    end
end