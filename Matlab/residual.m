function  [fy,f_p1,s] = residual(s,Y)

if  s.sol>0
    s.ro = s.Q(1:s.l_unk,:)*Y;   
    s.ro(s.ro<s.min) = s.min;
    s.ro(s.ro>s.max) = s.max;
    s.fi = s.Q(s.l_unk+1:s.l_unk*2,:)*Y;
end

if  s.corr
    s.K  = s.Q(2*s.l_unk+1:end,:)*Y;
    s.K_exp = 1 + s.I_K*s.K;
    h = s.c0^2*s.K_exp.*s.ro.^(s.g-1)/(s.g-1);
else
    if  s.avdW*s.bvdW==0  % ideal gas model
        h = s.c0^2*(s.ro.^(s.g-1))/(s.g-1);
    else   % Van der Waals gas model
        r = s.ro;
        b = s.r0*s.bvdW;
        k = (s.P0+s.r0^2*s.avdW)/(s.g-1);
        h = k*((1-b)./(1-r*b)).^s.g.*(s.g-r*b)/s.r0.*r.^(s.g-1) - 2*s.r0*s.avdW*r;
    end
    
end

if  s.free && s.pan>1   % FIXME (s.l_per index)
    s.gamma(1:s.nn) = s.fi(s.wake,:)-s.fi(s.WAKE,:);
end    

for d = 1:s.N
    if  s.pan>1 || s.N<3
        s.V(:,d) = s.G{d}*s.fi-s.GG{d}*s.gamma;
    else
        s.V(:,d) = s.GG{d}*s.fi;
    end
end

DL1 = (s.V(:,1)-s.Vg(:,1)).*(s.G{1}*s.ro);
for d = 2:s.N
    DL1 = DL1 + (s.V(:,d)-s.Vg(:,d)).*(s.G{d}*s.ro);
end

V2    = sum(s.V.^2-2*s.V.*s.Vg,2);
s.sv  = sqrt(sum((s.V-s.Vg).^2,2)); 
s.v2  = (s.JJ*s.sv.^2)./sum(s.JJ,2);
r     = s.r0*(s.ro);
s.c   = sqrt(s.g./(r.*(1-r*s.bvdW)).*(s.P0+s.avdW*s.r0^2).*(s.ro.*(1-s.r0*s.bvdW)./(1-r*s.bvdW)).^s.g - 2*s.avdW*r); 
s.M   = s.sv./mean(s.c(s.t3'-s.l_per),1)';
s.Mn  = s.Mb*s.M;
s.lin = length(find(s.Mn>1.01))<2^s.N;
s.upw = s.Lrif.*(s.eps_sub*s.M+s.eps_sup*real(sqrt(s.M.^2-1)));
s.RO  = mean(s.ro(s.t3-s.l_per),2) - s.upw.*DL1./s.sv;

F = 0;
if  s.ALE==1
    for d = 1:s.N
        F = F + s.M_x{d}*((s.V(:,d)-s.Vg(:,d)).*s.RO); 
    end
    s.tra = 0;
elseif s.ALE==2
    for d = 1:s.N
        F = F + s.M_x{d}*(s.V(:,d).*s.RO) + s.II*((s.G{d}*s.ro).*s.Vg(:,d));
    end
    s.tra = sum(s.Vb.*s.nb,2);
end 

E = s.I*(s.h0-h)-s.II*V2/2;

F(s.far-s.l_per) = 0;

for d = 1:s.N
    Vf_b(:,d) = s.Mb*(s.V(:,d)-s.Vg(:,d));
end

s.tra = s.tra + sum(Vf_b.*s.dn+s.Vt.*s.nb,2);

F(s.body-s.l_per) = F(s.body-s.l_per) + s.tra.*(s.ro(s.body-s.l_per));

T = 0;
if  s.corr
    shock = find(s.M(s.bup)>1.01 & s.M(s.bdo)<.99);
    vs =  (s.sv(s.bdo(shock))+s.sv(s.bup(shock)))/2-sqrt((2*s.c0^2+s.V0^2*(s.g-1))/(s.g+1));
    M2 = ((s.sv(s.bup(shock))-vs)/s.c0./s.RO(s.bup(shock)).^((s.g-1)/2)).^2;
    kk = (s.g+1)^(-s.g-1)*(s.g-1+2./M2).^s.g.*(2*s.g*M2-s.g+1);
    vn = sum((s.V(s.IND,:)-s.Vg(s.IND,:)).*s.nk,2);
    s.VI = s.V(s.IND,:)-s.Vg(s.IND,:) - spdiags(vn,0,s.l_k,s.l_k)*s.nk;
    s.K_exp(s.body2(shock)-s.l_per) = kk;
    for d = 1:s.N
        K_i = s.G{d}(s.IND,:)*s.K_exp;
        T = T - K_i.*s.VI(:,d);
        T(shock(:)) = T(shock(:)) + s.VI(shock(:),d).*K_i(shock(:));
    end
    T(abs(T)<1e-10)=0; 
end

if  s.sol==0
    
    s.ro(1:s.l_unk-s.l_w) = s.ro(1:s.l_unk-s.l_w) + s.dt*F./s.VOL;
    s.ro(s.WAKE-s.l_per)  = s.ro(s.wake-s.l_per);
    
    s.ro(s.ro<s.min) = s.min;
    s.ro(s.ro>s.max) = s.max;
    
    E = (s.I*(s.h0-h)-(s.II*V2/2))./s.VOL;
    
    s.fi(1:s.l_unk-s.l_w) = s.fi(1:s.l_unk-s.l_w) + s.dt*E;
    s.fi(s.WAKE-s.l_per)  = s.fi(s.WAKE-s.l_per)  + s.dt*(2*E(s.wake-s.l_per)-V2(s.wlo)+V2(s.wup))/2;
    f_p1 = 0;
    fy   = 0;

else

    ww = V2(s.wlo)/2-V2(s.wup)/2+h(s.WAKE-s.l_per)-h(s.wake-s.l_per);
    
    f_p1 = [F;E;ww;T(1:s.l_k*s.corr)];
    
    ff = [s.I*s.ro+s.I_fi*s.fi;s.IE*[s.ro;s.fi];s.fi(s.wake-s.l_per)-s.fi(s.WAKE-s.l_per);s.K(1:s.l_k*s.corr)]-s.b-s.dt*(s.b_p1^s.eul)*f_p1;
    fy = s.P*([ ff(1:s.l_equ) ; s.ro(s.wake-s.l_per).*s.K_exp(s.wake-s.l_per).^(1/s.g)-s.ro(s.WAKE-s.l_per).*s.K_exp(s.WAKE-s.l_per).^(1/s.g) ; ff(s.l_equ+1:end) ]);
    
end