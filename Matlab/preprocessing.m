function s = preprocessing(name)

time0 = tic;
disp('Preprocessing...')

ID = fopen(name);
tt = fscanf(ID,'%c');
fclose(ID);

[bb,~] = strread(tt,'%*s %s %*s %s','delimiter',':');

load(char(bb{1}));

eval(['s.p    = ' char(bb{2}) ';']);
eval(['t      = ' char(bb{3}) ';']);
eval(['s.body = ' char(bb{4}) ';']);
eval(['s.wake = ' char(bb{5}) ';']);
eval(['s.far  = ' char(bb{6}) ';']);

s.w_no = [];
s.w_si = [];
s.per_lo = [];
s.per_up = [];
if  ~isempty(bb{7}) && ~isempty(bb{8})
    eval(['s.per_up = ' char(bb{7}) ';']);
    eval(['s.per_lo = ' char(bb{8}) ';']);
    [~,ord] = sort(sum(s.p(s.per_up,end-1:end),2));
    s.per_up = s.per_up(ord);
    [~,ord] = sort(sum(s.p(s.per_lo,end-1:end),2));
    s.per_lo = s.per_lo(ord);
    s.w_no = intersect(s.wake,s.per_lo);
    s.w_si = intersect(s.wake,s.per_up);
    s.wake =   setdiff(s.wake,s.per_lo);
    
    s.w_si = s.w_si(s.p(s.w_si,end)>min(s.p(:,end)));
    s.w_no = s.w_no(s.p(s.w_no,end)>min(s.p(:,end)));
    
    [~,ord] = sort(sum(s.p(s.w_si,end-1:end),2));
    s.w_si = s.w_si(ord);
    [~,ord] = sort(sum(s.p(s.w_no,end-1:end),2));
    s.w_no = s.w_no(ord);
end
% max(abs(s.p(s.w_si,1)+s.p(s.w_no,1)))
% max(abs(s.p(s.w_si,2)-s.p(s.w_no,2)))
% max(abs(s.p(s.w_si,3)-s.p(s.w_no,3)))
% max(abs(s.p(s.per_up,1)+s.p(s.per_lo,1)))
% max(abs(s.p(s.per_up,2)-s.p(s.per_lo,2)))
% max(abs(s.p(s.per_up,3)-s.p(s.per_lo,3)))
% return

s.g       = str2double(bb{9 });
s.avdW    = str2double(bb{10});
s.bvdW    = str2double(bb{11});
s.P0      = str2double(bb{12});
s.r0      = str2double(bb{13});
s.eps_sub = str2double(bb{14});
s.eps_sup = str2double(bb{15});
s.vg      = str2num(   bb{16}); s.vg = s.vg';
s.wg      = str2num(   bb{17});
s.pole    = str2num(   bb{18});

s.max_rot = str2double(bb{19});

s.free    = str2double(bb{20});
s.ALE     = str2double(bb{21});
s.corr    = str2double(bb{22});
s.div     = str2double(bb{23});
s.dt      = str2double(bb{24});
s.step    = str2double(bb{25});
s.one     = str2double(bb{26});
s.iter    = str2double(bb{27});
s.Rho     = str2double(bb{28});
s.sol     = str2double(bb{29});
s.toll    = str2double(bb{30});
s.rest    = str2double(bb{31});
s.maxr    = str2double(bb{32});

s.mode = {};
if  length(bb)>32
    shape = load( char(bb{33}));
    m = str2num(bb{34});
    s.resp_type = str2double(bb{35});
    if  s.resp_type==1
        q = load([char(bb{33}(1:end-4)) 'magn']);
        s.magn = q(:,repmat(1:2,1,length(m))+reshape(repmat(m-1,2,1),2*length(m),1)'*2);
    else
        mck = load([char(bb{33}(1:end-4)) 'mck']);
        s.mck = mck(:,m);
    end
    s.mode = shape(:,repmat(1:3,1,length(m))+reshape(repmat(m-1,3,1),3*length(m),1)'*3);
end

s.c0 = sqrt(s.g./(s.r0*(1-s.r0*s.bvdW))*(s.P0+s.avdW*s.r0^2) - 2*s.avdW*s.r0);
s.V0 = norm(s.vg);
s.M0 = s.V0/s.c0;
s.h0 = (s.P0+s.r0^2*s.avdW)/(s.g-1)*(s.g-s.r0*s.bvdW)/s.r0 - 2*s.avdW*s.r0;

if s.one<2, s.one = s.step; end

s.l_per = 0;
if  ~isempty(s.per_lo)
    ind = 1:length(s.p);
    map = ind';
    
    [a,b] = ismember(ind,s.per_lo);
    map(a) = b(a);
    map(setdiff(ind,find(a))) = length(s.per_lo)+1:length(s.p);
    
    s.p = s.p([ s.per_lo ; setdiff(ind',s.per_lo) ],:);
    t = map(t);
    s.wake = map(s.wake);
    s.w_si = map(s.w_si);
    s.w_no = map(s.w_no);
    s.body = map(s.body);
    s.far  = map(s.far);
    s.far  = s.far(s.far>length(s.per_lo));
    s.per_lo = map(s.per_lo);
    s.per_up = map(s.per_up);
    s.l_per  = length(s.per_lo);
end

% figure
% hold on
% % triplot(t,s.p(:,1),s.p(:,2),'-k')
% plot3(s.p(s.far,1),s.p(s.far,2),s.p(s.far,3),'.b')
% % plot3(s.p(s.wake,1),s.p(s.wake,2),s.p(s.wake,3),'.c')
% plot3(s.p(s.body,1),s.p(s.body,2),s.p(s.body,3),'.g')
% % plot(s.p(s.per_up,1),s.p(s.per_up,2),'.y')
% % plot(s.p(s.per_lo,1),s.p(s.per_lo,2),'.r')
% axis equal
% % s.p(s.per_up,1)-s.p(s.per_lo,1)
% return

[ s.l_p , s.N ] = size(s.p);

s.te = intersect(s.body,s.wake);

if  s.free
    if  s.N>2
        pp = s.p(s.te,:);
        di = sqrt(sum(pp.^2,2));
        vv = pp(di==max(di),:)-pp(di==min(di),:);
        [~ ,ind] = sort(pp*vv');
        s.te = s.te(ind);
    end
    s.wake = s.te;
    s.ind_te = 1:length(s.te)+(2-s.N);
end

s.l_t = length(t);
s.l_w = length(s.wake);
s.l_b = length(s.body);
s.l_e = length(s.far);

%---------------------------FIXING VOLUMES---------------------------%
if  s.N>2    
    v34 = s.p(t(:,3),2).*s.p(t(:,4),3) - s.p(t(:,3),3).*s.p(t(:,4),2);
    v24 = s.p(t(:,2),2).*s.p(t(:,4),3) - s.p(t(:,2),3).*s.p(t(:,4),2);
    v23 = s.p(t(:,2),2).*s.p(t(:,3),3) - s.p(t(:,2),3).*s.p(t(:,3),2);
    v14 = s.p(t(:,1),2).*s.p(t(:,4),3) - s.p(t(:,1),3).*s.p(t(:,4),2);
    v13 = s.p(t(:,1),2).*s.p(t(:,3),3) - s.p(t(:,1),3).*s.p(t(:,3),2);
    v12 = s.p(t(:,1),2).*s.p(t(:,2),3) - s.p(t(:,1),3).*s.p(t(:,2),2);
    det = sum(reshape(s.p(t,1),s.l_t,4).*...
        [+v34-v24+v23  -v34+v14-v13  +v24-v14+v12  -v23+v13-v12],2);
else
    det = + s.p(t(:,2),1).*s.p(t(:,3),2) - s.p(t(:,2),2).*s.p(t(:,3),1)...
          - s.p(t(:,1),1).*s.p(t(:,3),2) + s.p(t(:,1),2).*s.p(t(:,3),1)...
          + s.p(t(:,1),1).*s.p(t(:,2),2) - s.p(t(:,1),2).*s.p(t(:,2),1); 
end

s.t = t; s.t(det<0,[1 2]) = t(det<0,[2 1]); 

clear t det v12 v13 v14 v23 v24 v34

%----------------------------CONNECTIVITY----------------------------%
map = -ones(s.l_p,1)*s.l_p*2;
map(s.body) = 1:s.l_b; 

tri = map(freeBoundary(TriRep(s.t,s.p)));
s.faces = (tri(sum(tri,2)>0,:));

map = -ones(s.l_p,1)*s.l_p*2;
map(s.far) = 1:s.l_e; 

tri = map(freeBoundary(TriRep(s.t,s.p)));
s.fac_f = (tri(sum(tri,2)>0,:));

A = logical(sparse(repmat(1:s.l_t,1,s.N+1),reshape(s.t,numel(s.t),1),ones(s.l_t*(s.N+1),1),s.l_t,s.l_p));

v_nodal = -cross(repmat(s.wg,s.l_p,1),[s.p zeros(s.l_p,3-s.N)])-repmat(s.vg',s.l_p,1);
v_nodal = v_nodal(:,1:s.N);

mod = sqrt(sum(v_nodal.^2,2));
v_n = spdiags(1./mod,0,s.l_p,s.l_p)*v_nodal;

%WAKE NORMALS
nw = zeros(s.l_w,s.N);
if  ~isempty(s.wake)
    if  s.free && s.N<3
        nw = [s.vg(2) -s.vg(1)]./norm(s.vg);
    else
        ww = [s.wake;s.w_no];
        ind = zeros(s.l_p,1);
        ind(ww) = 1;
        ind = logical(ind);
        for i = 1:length(ww)
            nod = unique(s.t(A(:,ww(i)),:));
            ind(ww(i)) = 0;
            nod = nod(ind(nod));
            ind(ww(i)) = 1;
            if  s.N>2
                dp = s.p(nod,:)-repmat(s.p(ww(i),:),length(nod),1);
                if  s.free
                    nW = cross(dp(1,:),v_n(ww(i),:));
                else
                    nW = cross(dp(1,:),dp(2,:));
                    if  norm(nW)<norm(dp(1,:))*norm(dp(2,:))/10
                        nW = cross(dp(1,:),dp(3,:));
                    end
                end
            else
                nW = cross([s.p(nod(1),:)-s.p(ww(i),:) 0],[ 0 0 1 ]);
            end
            [~,ii] = max(abs(nW));
            nw(i,:) = sign(nW(ii))*nW(1:s.N)./norm(nW);
        end
    end
end
nw_no = nw(s.l_w+1:end,:);
qq_no = -sum(s.p(s.w_no,:).*nw_no,2);

nw = nw(1:s.l_w,:);
qq = -sum(s.p(s.wake,:).*nw,2);

% figure
% hold on
% % plot3(s.p(s.far ,1),s.p(s.far ,2),s.p(s.far ,3),'.r')
% plot3(s.p(s.body,1),s.p(s.body,2),s.p(s.body,3),'.k')
% % plot3(s.p(s.wake,1),s.p(s.wake,2),s.p(s.wake,3),'.b')
% quiver3(s.p(s.wake,1),s.p(s.wake,2),s.p(s.wake,3),nw(:,1),nw(:,2),nw(:,3),'-r')
% axis equal
% view([0 -1 0])
% zoom(2)

s.pt = zeros(s.l_t,s.N);  % element barycenters
for d = 1:s.N;
    x = s.p(:,d);
    s.pt(:,d) = mean(x(s.t),2);
end

s.Vg = cross(repmat(s.wg,s.l_t,1),[ s.pt zeros(s.l_t,3-s.N)])+repmat(s.vg',s.l_t,1);
s.Vg = s.Vg(:,1:s.N);

s.Vb = cross(repmat(s.wg,s.l_b,1),[ s.p(s.body,:) zeros(s.l_b,3-s.N)])+repmat(s.vg',s.l_b,1);
s.Vb = s.Vb(:,1:s.N);

p2 = ones(s.l_p,1);
p2([s.body;s.wake;s.w_no]) = 0;

s.wup = zeros(s.l_w,1);  % wake upper upwind elements
s.wlo = zeros(s.l_w,1);  % wake lower upwind elements   
for i = 1:s.l_w
    ele = find(A(:,s.wake(i)));
    tri = ele(sum(p2(s.t(ele,:)),2)<2);
    d   = qq(i)+s.pt(tri,:)*nw(i,:)';
    
%     ind = find(s.wake(i)==s.w_si);
%     if  ~isempty(ind)
%         if  (s.p(s.w_no(ind),:)*v_n(s.w_no(ind),:)')>(s.p(s.w_si(ind),:)*v_n(s.w_si(ind),:)')
%             tri = conn{s.w_no(ind)};
%             d   = [ (qq_no(i)+s.pt(conn{s.w_no(ind)},:)*nw_no(ind,:)')];
%         end
%     end
    
    up = find(d>0); % upper elements
    lo = find(d<0); % lower elements
    dd = repmat(s.p(s.wake(i),:),length(tri),1)-s.pt(tri,:);
    if  s.N>2
        dt = dd*cross(nw(i,:),v_n(s.wake(i),:))';
    else
        dt = dd*nw(i,:)';
    end
    dv = dd*v_n(s.wake(i),:)';
    ta = dv./abs(dt); 
    [ ~ , pos_up ] = max(ta(up));
    [ ~ , pos_lo ] = max(ta(lo));
    if  ~isempty(up) && ~isempty(lo) 
        s.wup(i) = tri(up(pos_up)); 
        s.wlo(i) = tri(lo(pos_lo));
    end    
end

ind = find(s.wup);

s.wake = s.wake(ind);
s.wlo  = s.wlo(ind);
s.wup  = s.wup(ind);

nw = nw(ind,:);
qq = qq(ind);

% figure
% hold on
% % plot(s.p(s.per_up,1),s.p(s.per_up,2),'ob')
% % plot(s.p(s.per_lo,1),s.p(s.per_lo,2),'og')
% % plot(s.p(s.wake  ,1),s.p(s.wake  ,2),'.r')
% % quiver(s.p(s.wake,1),s.p(s.wake,2),nw(:,1),nw(:,2))
% % triplot(s.t(s.wlo,:),s.p(:,1),s.p(:,2),'-r','linewidth',2);
% % plot3(s.p(s.wake,1),s.p(s.wake,2),s.p(s.wake,3),'.r')
% quiver3(s.p(s.wake,1),s.p(s.wake,2),s.p(s.wake,3),nw(:,1),nw(:,2),nw(:,3))
% tetramesh(s.t(s.wup,:),s.p);
% axis equal
% return

s.body2 = setdiff(s.body,s.wake);

s.l_b2= length(s.body2);
s.l_w = length(s.wake);
s.l_k = s.l_b2+2*s.l_w;

s.WAKE = (s.l_p+(1:s.l_w))';

s.nn = s.l_w*(s.N>2)+2*(s.N<3);

if  s.free
    
    p3 = zeros(s.l_p,1);
    p3(s.body) = 1;
    p3(s.wake) = 2;
    s.on = find(sum(p3(s.t),2)>=(s.N-1)*2+1);

    ind_i = [];
    ind_j = [];
    ind_a = [];
    for i = 1:s.l_w
        ele = find(A(:,s.wake(i)));
        ele_on = intersect(ele,s.on);
        ele_on = ele_on((s.pt(ele_on,:)*v_n(s.wake(i),:)')<(s.p(s.wake(i),:)*v_n(s.wake(i),:)'));
        ll = length(ele_on);
        ind_i = [ ind_i ; i*ones(ll,1) ];
        ind_j = [ ind_j ;    ele_on    ];
        ind_a = [ ind_a ; ones(ll,1)/ll];
    end
    s.MM1 = sparse(s.wake(ind_i),ind_j,ind_a,s.l_p,s.l_t);
    s.MM1 = s.MM1(:,s.on);  

    tutti = [reshape(s.t(:,1:3),3*s.l_t,1) vertcat(s.t(:,2),s.t(:,3),s.t(:,1))];
    tutti = double([tutti;tutti(1:end*(s.N>2),1) repmat(s.t(:,end),3*(s.N>2),1)]);

    ver = sum(tutti,2)+tutti(:,1).*tutti(:,2);

    [~,m,~] = unique(ver,'first');

    s.edge = tutti(m,:);

    s.xte = [s.p(s.wake,:) zeros(1,3-s.N)];
    if abs(min(s.p(:,end)))<1e-3, s.xte(1,end)=-1; end

    xx = (s.p(s.edge(:,1),:)+s.p(s.edge(:,2),:))*s.vg(1:s.N)/2;

    s.edge = s.edge(xx<=max(s.xte*s.vg),:);

    s.l_l = length(s.edge);
    
    xx1 = [s.p(s.edge(:,1),:) zeros(s.l_l,3-s.N)];
    xx2 = [s.p(s.edge(:,2),:) zeros(s.l_l,3-s.N)];
    xxd = xx2-xx1;
    
    v1 = -s.vg'./sqrt(sum(s.vg.^2));
    
    if s.N>2
       v2 = s.xte(1:end-1,:)-s.xte(2:end,:);
    else
       v2 = [ 0 0 -1 ];
    end

    s.nfree = [v1(2)*v2(:,3) -v1(1)*v2(:,3) v1(1)*v2(:,2)-v1(2)*v2(:,1) ];

    mod = sqrt(sum(s.nfree.^2,2));

    s.nfree = spdiags(1./mod,0,s.nn-1,s.nn-1)*s.nfree;
    s.qfree = -sum(s.nfree.*s.xte(1:end-(s.N-2),:),2); 

    delta = repmat(max(-s.p*s.vg(1:s.N)/s.V0)*2*v1,2,1);

    dd = [2:3 1:3]; 
    mm = max(-s.p*s.vg(1:s.N)/s.V0);
    for i = s.ind_te
        
        if  s.N>2
            coor = [ s.xte(i:i+1,:) ; s.xte(i+1:-1:i,:)+delta ];
        else
            tt = [ s.xte(1:2) -.5
                   s.xte(1:2) +.5 ];
            coor = [ tt ; tt([2 1],:)+delta ];
        end
        
        ss1 = s.qfree(i)+sum(xx1*s.nfree(i,:)',2);
        ss2 = s.qfree(i)+sum(xx2*s.nfree(i,:)',2);

        few = find(ss1.*ss2<=0);    
        u  = abs(ss1(few))./abs(ss1(few)-ss2(few));        
        xi = zeros(length(few),3);
        for d = 1:s.N
            xi(:,d) = xx1(few,d)+u.*xxd(few,d); 
        end     
        ss = zeros(size(xi));
        for k = 1:3  % edges of quadrilateral minus 1
            v1 = coor(k+1,:)-coor(k,:);
            for d = 1:s.N
                ss(:,k) = ss(:,k)+(v1(dd(d  ))*(xi(:,dd(d+1))-coor(k,dd(d+1)))-...
                                   v1(dd(d+1))*(xi(:,dd(d  ))-coor(k,dd(d  ))))*s.nfree(i,d);
            end
        end
        ss = [ ss mod(i)*mm*4-sum(ss,2) ];        
        less = find(sum(ss>0,2)==4);
        s.lati{i} = few(less);
        s.inter{i} = xi(less,:);
        s.segno{i} = sign(ss2(s.lati{i})); 
        s.segno{i}(s.segno{i}==0) = 1;
    end
else
    for d = 1:s.N
        s.GG{d} = 0;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
s.t3 = s.t;
[a ,b ] = ismember(reshape(s.t3,numel(s.t3),1),s.per_lo);
s.t3(a) = s.per_up(b(a));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% tt = tic;
t2 = s.t3;
for i = 1:s.l_w 
    ele = find(A(:,s.wake(i)));
    low = ele((qq(i)+s.pt(ele,:)*nw(i,:)')<0);% lower elements
    ind = find(s.wake(i)==s.w_si);
    if  ~isempty(ind)
        ele = find(A(:,s.w_no(ind)));
        low = [low;ele((qq_no(ind)+s.pt(ele,:)*nw_no(ind,:)')<0)];
    end
    for j = 1:s.N+1
        t2(low(t2(low,j)==s.wake(i)),j) = s.l_p+i;  % fix connectivity
    end
end
% toc(tt)
% return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if  s.corr && s.l_b>1
    s.bup = zeros(length(s.body2),1);  % body   upstream elements
    s.bdo = zeros(length(s.body2),1);  % body downstream elements
    for i = 1:length(s.body2)
        ele = find(A(:,s.body2(i)));
        el = ele(sum(p2(s.t(ele,:)),2)<2);
        dd = repmat(s.p(s.body2(i),:),length(el),1)-s.pt(el,:);
        if  s.N>2
            dt = dd*cross(nw(1,:),v_n(s.body2(i),:))';
        else
            dt = dd*nw(1,:)';
        end
        ta = dd*round(v_n(s.body2(i),:)')./abs(dt);
        [ ~ , pos_up ] = max(ta);
        [ ~ , pos_do ] = min(ta);
        s.bup(i) = el(pos_up);
        s.bdo(i) = el(pos_do);
    end
    
    s.IND = [ s.bup ; s.wup ; s.wlo ];  % elements for entropy transport
    
    node = s.IND*0;
    for i = 1:s.l_k
        node(i) = max([1 find(p2(s.t(s.IND(i),:))>0)]);
    end
    s.indk = (1:s.l_k)'+(node-1)*s.l_k; % locating normals at body
    
%         figure
%         hold on
%         plot(s.p(s.body,1),s.p(s.body,2),'.k')
%         plot(s.p(s.wake,1),s.p(s.wake,2),'.r')
% %         tetramesh(s.t(s.bup,:),s.p)
%         % triplot(s.t(s.IND,:),s.p(:,1),s.p(:,2),'-b')
%         triplot(s.t(s.wlo,:),s.p(:,1),s.p(:,2),'-g')
%         % triplot(s.t(s.bdo,:),s.p(:,1),s.p(:,2),'-y')
%         axis equal
%         return
end

%---------------------------Metrics----------------------------%

N1 = s.N+1; 
NN = s.l_t*N1^2;

s.v_i = repmat(1:s.l_t,1,s.N+1);

s.l_equ = s.l_p-s.l_per;   % # of equations
s.l_unk = s.l_equ+s.l_w;     % # of unknows

s.v_j = reshape(s.t ,N1*s.l_t,1);
s.v_k = reshape(t2  ,N1*s.l_t,1);
s.v_h = reshape(s.t3,N1*s.l_t,1);
    
peso = sum(1./(1:N1))/N1^2;
pesi = ones(N1^2,1)*(1/N1-peso)/s.N;
pesi(1:(s.N+2):end) = peso;

blocco = reshape(repmat((0:s.N)'*s.l_t+1,1,s.N+1),N1^2,1);
cumulo = floor((0:NN-1)'/N1^2);

s.v_J = s.v_j(repmat(sort(blocco),s.l_t,1)+cumulo);
s.v_H = s.v_h(repmat(sort(blocco),s.l_t,1)+cumulo);
s.v_K = s.v_k(repmat(     blocco ,s.l_t,1)+cumulo);

s.E_v = sparse(1:s.l_t*N1,s.v_i,ones(1,s.l_t*N1)/N1,s.l_t*N1,s.l_t);
s.E_p = sparse(1:NN,floor((N1^2:NN+N1^2-1)/N1^2),pesi(repmat(1:N1^2,1,s.l_t)),NN,s.l_t);

s.Mbb = sparse(reshape(s.faces,numel(s.faces),1),...
               repmat(1:length(s.faces),1,s.N),...
               ones(numel(s.faces),1)/factorial(s.N));
           
s.Mff = sparse(reshape(s.fac_f,numel(s.fac_f),1),...
               repmat(1:length(s.fac_f),1,s.N),...
               ones(numel(s.fac_f),1)/factorial(s.N));

s = deform(s);

s.nw = nw;

s.Mb = spdiags(1./sum(s.II(s.body-s.l_per,:),2),0,s.l_b,s.l_b)*s.II(s.body-s.l_per,:);

s.VOL2 = sum(sparse(s.v_k-s.l_per,s.v_i,s.E_v*s.Lrif.^s.N,s.l_unk,s.l_t),2);

s.VOL = sum(s.I,2);

s.I_fi = sparse(s.l_equ,s.l_unk);

s.uni = sparse([1:s.l_w 1:s.l_w],[s.wake;s.WAKE]'-s.l_per,[ones(1,s.l_w) -ones(1,s.l_w)],s.l_w,s.l_unk);

% interpolation matrix from K dof to all dof
s.I_K = sparse([s.body2;s.wake;s.WAKE]-s.l_per,[(1:s.l_b2) s.l_b2+(1:s.l_w) s.l_b2+s.l_w+(1:s.l_w)],ones(s.l_k,1),s.l_unk,s.l_k);

if s.div==0
   s = rmfield(s,{'E_p','E_v','v_J','v_K','v_H','v_j','v_i','v_h'});
end

disp(['Done in ' num2str(toc(time0),3) ' seconds'])
disp(' ')