pp  = s.p;
ang = pi/4;

figure
hold on
for i = 1:8
    pp(:,1:2) = [pp(:,1)*cos(ang)+pp(:,2)*sin(ang) pp(:,2)*cos(ang)-pp(:,1)*sin(ang)];
    trisurf(s.faces,pp(s.body,1),pp(s.body,2),pp(s.body,3),s.Mn,...
            'EdgeColor','none','FaceColor','interp','Facealpha',.8);
    plot3(pp(s.wake,1),pp(s.wake,2),pp(s.wake,3)+s.fi(s.wake-s.l_per)/30,'.r',...
          pp(s.wake,1),pp(s.wake,2),pp(s.wake,3)+s.fi(s.WAKE-s.l_per)/30,'.b');           
end
axis off
axis equal
view([-5 4 3])
rotate3d on