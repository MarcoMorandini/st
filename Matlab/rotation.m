function vr = rotation(w,v)

t = norm(w); % angle
u = w/t;     % axis

x = u(1); y = u(2); z = u(3);

ux = [ 0 -z  y
       z  0 -x
      -y  x  0 ];
  
uc = [ x^2  x*y  x*z
       x*y  y^2  y*z
       x*z  y*z  z^2 ];

% Rodrigues' rotation formula
vr = (cos(t)*eye(3) + sin(t)*ux + (1-cos(t))*uc)*v;