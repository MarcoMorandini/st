function [X,conv] = solver(s,Y,f0)
if  s.lin
    x = -s.U\(s.L\f0); ff = -1;
else
    switch s.sol
        case 1
            x = -s.AA\f0; ff = -2;
        case 2
            [x,ff] = gmres(s.AA ,-f0,s.rest,s.toll,s.maxr,s.L,s.U);
        case 3
            [x,ff] = gmres(@Afun,-f0,s.rest,s.toll,s.maxr,s.L,s.U);
    end
end
conv = [s.conv ; ff];
X = (Y+x);

    function z = Afun(w)
        ep  = 1.e-6;
        n_w = norm(w);
        n_x = norm(Y);
        if  n_w==0
            z = zeros(size(Y));
            return
        end
        ep = ep/n_w;
        if n_x>0
            ep = ep*n_x;
        end
        [fy,~] = residual(s,Y+ep*w);
        z = (fy-f0)/ep;
    end
end